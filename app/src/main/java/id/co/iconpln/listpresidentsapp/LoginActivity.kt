package id.co.iconpln.listpresidentsapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnShowLogin.setOnClickListener {
            checkfield()
        }
    }

    fun checkfield(){

        when{
            etUsername.text.isNullOrEmpty() -> etUsername.error ="Username tidak boleh kosong"
            etPassword.text.isNullOrEmpty() -> etPassword.error = "Password tidak boleh kosong"
            etPassword.text.length < 7 -> etPassword.error="Password Tidak Boleh Kurang dari 7 Karakter"
            !Patterns.EMAIL_ADDRESS.matcher(
                etUsername.text).matches() -> etUsername.error = "Format email salah"
            else ->{
                doLogin(etUsername.text.toString(), etPassword.text.toString())
            }
        }
    }

    private fun doLogin(username: String, password: String) {

        if (username.equals("user@mail.com", false)
            && password.equals("password", false)) {
            val mainActivity = Intent(this, MainActivity::class.java)
            startActivity(mainActivity)
            finish()

        } else {
            Toast.makeText(this, "Username atau Password Salah", Toast.LENGTH_SHORT).show()
        }
    }
}

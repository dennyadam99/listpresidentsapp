package id.co.iconpln.listpresidentsapp

data class President (
    var name: String="",
    var desc: String="",
    var photo: String=""
)
package id.co.iconpln.listpresidentsapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

class ListViewPresidentAdapter(val context: Context, val listPresident: ArrayList<President>) :
    BaseAdapter() {

    override fun getView(index: Int, view: View?, viewGroup: ViewGroup?): View {
        val viewLayout = LayoutInflater.from(context)
            .inflate(R.layout.item_list_president, viewGroup, false)

        val viewHolder = ViewHolder(viewLayout)
        val president = getItem(index) as President
        viewHolder.bind(context, president)
        return viewLayout
    }

    override fun getItem(index: Int): Any {
        return listPresident[index]
    }

    override fun getItemId(index: Int): Long {
        return index.toLong()
    }

    override fun getCount(): Int {
        return listPresident.size
    }

    private inner class ViewHolder(view: View) {
        private val tvPresidentName: TextView = view.findViewById(R.id.tvPresidentName)
        private val tvPresidentDesc: TextView = view.findViewById(R.id.tvPresidentDescription)
        private val ivPresidentPhoto: ImageView = view.findViewById(R.id.ivPresidentPhoto)

        fun bind(context: Context, president: President) {
            tvPresidentName.text = president.name
            tvPresidentDesc.text = president.desc

            Glide.with(context).load(president.photo).into(ivPresidentPhoto)

        }
    }
}
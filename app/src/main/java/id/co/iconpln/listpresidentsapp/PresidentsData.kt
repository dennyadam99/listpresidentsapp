package id.co.iconpln.listpresidentsapp

object PresidentsData {
    val listDataPresident: ArrayList<President>
        get() {
            val list = ArrayList<President>()
            for (data in dataPresidents){
                val president = President()
                president.name = data[0]
                president.desc = data[1]
                president.photo = data[2]
                list.add(president)
            }
            return list
        }

    private var dataPresidents = arrayOf(
        arrayOf(
            "Soeakarno",
            "Dr. Ir. H. Soekarno adalah Presiden pertama Republik Indonesia yang menjabat pada periode 1945–1967. Ia memainkan peranan penting dalam memerdekakan bangsa Indonesia dari penjajahan Belanda. Ia adalah Proklamator Kemerdekaan Indonesia yang terjadi pada tanggal 17 Agustus 1945. ",
            "https://upload.wikimedia.org/wikipedia/commons/0/01/Presiden_Sukarno.jpg"
        ),
        arrayOf(
            "Soeharto",
            "Jenderal Besar TNI H. M. Soeharto, adalah Presiden kedua Indonesia yang menjabat dari tahun 1967 sampai 1998, menggantikan Soekarno.",
            "https://upload.wikimedia.org/wikipedia/commons/5/59/President_Suharto%2C_1993.jpg"
        ),
        arrayOf(
            "B.J. Habibie",
            "Prof. Dr. Ing. H. Bacharuddin Jusuf Habibie, FREng (lahir di Parepare, Sulawesi Selatan, 25 Juni 1936 – meninggal di Jakarta, 11 September 2019 pada umur 83 tahun) adalah Presiden Republik Indonesia yang ketiga. Sebelumnya, B.J. Habibie menjabat sebagai Wakil Presiden Republik Indonesia ke-7, menggantikan Try Sutrisno. B. J. Habibie menggantikan Soeharto yang mengundurkan diri dari jabatan presiden pada tanggal 21 Mei 1998.",
            "https://upload.wikimedia.org/wikipedia/commons/9/95/Foto_Presiden_Habibie_1998.jpg"
        ),
        arrayOf(
            "Abdurrahman Wahid",
            "Dr. K. H. Abdurrahman Wahid atau yang akrab disapa Gus Dur adalah tokoh Muslim Indonesia dan pemimpin politik yang menjadi Presiden Indonesia yang keempat dari tahun 1999 hingga 2001. Ia menggantikan Presiden B.J. Habibie setelah dipilih oleh Majelis Permusyawaratan Rakyat hasil Pemilu 1999.",
            "https://upload.wikimedia.org/wikipedia/commons/3/35/President_Abdurrahman_Wahid_-_Indonesia.jpg"
        ),
        arrayOf(
            "Megawati Soekarnoputri",
            "Dr. Hj. Dyah Permata Megawati Setyawati Soekarnoputri atau umumnya lebih dikenal sebagai Megawati Soekarnoputri adalah Presiden Indonesia yang kelima yang menjabat sejak 23 Juli 2001 sampai 20 Oktober 2004.",
            "https://upload.wikimedia.org/wikipedia/commons/8/88/President_Megawati_Sukarnoputri_-_Indonesia.jpg"
        ),
        arrayOf(
            "Susilo Bambang Yudhoyono",
            "Jenderal TNI Prof. Dr. H. Susilo Bambang Yudhoyono, M.A., GCB., AC. adalah Presiden Indonesia ke-6 yang menjabat sejak 20 Oktober 2004 hingga 20 Oktober 2014.Ia adalah Presiden pertama di Indonesia yang dipilih melalui jalur pemilu. Ia, bersama Wakil Presiden Muhammad Jusuf Kalla, terpilih dalam Pemilu Presiden 2004.",
            "https://upload.wikimedia.org/wikipedia/commons/5/58/Presiden_Susilo_Bambang_Yudhoyono.png"
        ),
        arrayOf(
            "Joko Widodo",
            "Ir. H. Joko Widodo atau Jokowi adalah Presiden ke-7 Indonesia yang mulai menjabat sejak 20 Oktober 2014. Ia terpilih bersama Wakil Presiden Muhammad Jusuf Kalla dalam Pemilu Presiden 2014 dan kembali terpilih bersama Wakil Presiden Ma'ruf Amin dalam Pemilu Serentak 2019.",
            "https://upload.wikimedia.org/wikipedia/commons/b/be/Joko_Widodo_2019_official_portrait.jpg"
        ),
        arrayOf(
            "Kim Jong Un",
            "Kim Jong-un adalah Pemimpin Tertinggi Republik Demokratik Rakyat Korea, atau yang lebih dikenal dengan un jong kim. Ia adalah putra Kim Jong-il dan cucu dari Kim Il-sung. Sebelum menjadi pemimpin Korea Utara, ia pernah menjabat sebagai Sekretaris Pertama Partai Buruh Korea, Ketua Pertama Komisi Militer Sentral, Panglima Tertinggi Tentara Rakyat Korea, dan anggota presidium Politbiro Partai Buruh Korea.",
            "https://upload.wikimedia.org/wikipedia/commons/3/3e/Kim_Jong-un_at_the_Workers%27_Party_of_Korea_main_building.png"
        ),
        arrayOf(
            "Donald Trump",
            "Donald John Trump adalah pebisnis, tokoh televisi realita, politikus, dan Presiden Amerika Serikat ke-45. Sejak 1971, ia memimpin The Trump Organization, perusahaan induk utama untuk semua usaha properti dan kepentingan bisnis lain miliknya.",
            "https://upload.wikimedia.org/wikipedia/commons/5/56/Donald_Trump_official_portrait.jpg"
        ),
        arrayOf(
            "Recep Tayyip Erdoğan",
            "Recep Tayyip Erdoğan adalah seorang politikus Turki yang menjabat sebagai Presiden Turki sejak 2014. Sebelumnya, ia menjabat Perdana Menteri Turki sejak 14 Maret 2003 sampai 28 Agustus 2014. Ia juga seorang pimpinan Adalet ve Kalkınma Partisi.",
            "https://upload.wikimedia.org/wikipedia/commons/6/65/Recep_Tayyip_Erdo%C4%9Fan_2019_%28cropped%29.jpg"
        )
    )
}


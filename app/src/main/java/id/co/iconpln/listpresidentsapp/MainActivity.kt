package id.co.iconpln.listpresidentsapp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val listPresident: ListView
        get() = lv_list_president

    private var list: ArrayList<President> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupActionBar()
        loadListBaseAdapter(this)
    }

    private fun setupActionBar() {
        supportActionBar?.title ="President List"
        supportActionBar?.setDisplayHomeAsUpEnabled((true))
    }

    private fun loadListBaseAdapter(context: Context) {
        list.addAll(PresidentsData.listDataPresident)

        val baseAdapter = ListViewPresidentAdapter(context, list)
        listPresident.adapter = baseAdapter
    }
}

